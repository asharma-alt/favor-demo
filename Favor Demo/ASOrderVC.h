//
//  ASOrderVC.h
//  Favor Demo
//
//  Created by Amit Sharma on 5/2/16.
//  Copyright © 2016 Amit Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASFeaturedContentPacket.h"

@interface ASOrderVC : UIViewController

- (instancetype)initWithFeaturedContent:(ASFeaturedContentPacket *)featuredContent;

@end
