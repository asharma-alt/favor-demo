//
//  ASAddNewLineItemCell.h
//  Favor Demo
//
//  Created by Amit Sharma on 5/3/16.
//  Copyright © 2016 Amit Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString * kAddNewLineItemCellReuseIdentifier = @"add new";

@protocol ASAddNewLineItemCellDelegate <NSObject>

- (void)addNewLineItem;

@end

@interface ASAddNewLineItemCell : UITableViewCell

- (void)prepareForReuseWithDelegate:(id<ASAddNewLineItemCellDelegate>)delegate;

@end
