//
//  ASFeaturedCell.m
//  Favor Demo
//
//  Created by Amit Sharma on 4/24/16.
//  Copyright © 2016 Amit Sharma. All rights reserved.
//

#import "ASFeaturedCell.h"
#import "NSCache+sharedCache.h"

@interface ASFeaturedCell ()

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UILabel *bottomLabel;
@property (weak, nonatomic) IBOutlet UILabel *fakeMenuButtonLabel;

@property (strong, nonatomic) NSString *currentImageURL;

@end

@implementation ASFeaturedCell

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    _fakeMenuButtonLabel.layer.borderWidth = 1.0f;
    _fakeMenuButtonLabel.layer.borderColor = [[UIColor whiteColor] CGColor];
    _fakeMenuButtonLabel.layer.cornerRadius = 2.0f;
}

- (void)prepareForReuseWithFeaturedContentPacket:(ASFeaturedContentPacket *)contentPacket{
    
    self.topLabel.text = [NSString stringWithFormat:@"%@ - $%.2f", contentPacket.menuItem.itemName, contentPacket.menuItem.priceInCents / 100.0f];
    self.bottomLabel.text = contentPacket.store.name;
    
    [self setBackgroundImageWithImageURL:contentPacket.imageURL];
}

// **** NOTE TO FAVOR ENGINEERS **** //
//
// This is a quick-and-dirty implementation of image loading / caching for the demo.
// In practice, I would probably use AFNetworking's UIImageView category.
// If nothing else, this would be cleaned up and moved to a UIImageView category or subclass.

- (void)setBackgroundImageWithImageURL:(NSString *)imageURL{
    
    if ([_currentImageURL isEqualToString:imageURL]) return;
    
    _backgroundImage.image = nil;
    _currentImageURL = imageURL;
    
    dispatch_queue_t asyncQueue = dispatch_queue_create("async_image_fetch", NULL);
    dispatch_async(asyncQueue, ^{
        
        NSURL *URL = [NSURL URLWithString:imageURL];
        UIImage *image = [[NSCache sharedCache] objectForKey:imageURL];
        
        if (image) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (self && [self.currentImageURL isEqualToString:imageURL]){
                    
                    _backgroundImage.image = image;
                    [self setNeedsLayout];
                }
            });
        }
        
        else{
            
            NSData *imageData = [NSData dataWithContentsOfURL:URL];
            image = [UIImage imageWithData:imageData];
            
            if (image) {
                
                [[NSCache sharedCache] setObject:image forKey:imageURL];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if (self && [self.currentImageURL isEqualToString:imageURL]){
                        
                        [UIView transitionWithView:self
                                          duration:0.5f
                                           options:UIViewAnimationOptionTransitionCrossDissolve | UIViewAnimationOptionCurveLinear
                                        animations:^{
                    
                                            _backgroundImage.image = image;
                                            [self setNeedsLayout];
                                        }
                                        completion:nil];
                    }
                });
            }
        };
    });
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated{
    
    if (selected)[self setSelected:NO animated:YES];
}

@end
