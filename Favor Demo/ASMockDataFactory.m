//
//  ASMockDataFactory.m
//  Favor Demo
//
//  Created by Amit Sharma on 5/8/16.
//  Copyright © 2016 Amit Sharma. All rights reserved.
//

#import "ASMockDataFactory.h"
#import "ASFeaturedContentPacket.h"

@interface ASMockDataFactory ()

@property (strong, nonatomic) NSMutableArray *mockFeaturedContentPackets;
@property (strong, nonatomic) NSMutableArray *mockMenuItems;

@end

@implementation ASMockDataFactory

#pragma mark - Featured Content Packets

- (NSArray *)generateEightMockFeaturedContentPackets{
    
    return [self eightRandomlyOrderedItemsFromMutableArray:self.mockFeaturedContentPackets];
}

- (NSArray *)mockFeaturedContentPackets{
    
    if (!_mockFeaturedContentPackets) {

    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"mock_featured_json_data" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    NSArray *dicts = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    NSMutableArray *featuredDeliveries = [NSMutableArray new];
    
    for (NSDictionary *dict in dicts) {
        
        ASFeaturedContentPacket *contentPacket = [ASFeaturedContentPacket new];
        ASStore *store = [ASStore new];
        ASMenuItem *menuItem = [ASMenuItem new];
        
        store.name = [dict objectForKey:@"store"];
        menuItem.itemName = [dict objectForKey:@"name"];
        contentPacket.imageURL = [dict objectForKey:@"image_url"];
        
        NSNumber *priceInCents = [dict valueForKey:@"price_in_cents"];
        menuItem.priceInCents = priceInCents.integerValue;
        
        contentPacket.store = store;
        contentPacket.menuItem = menuItem;
        
        [featuredDeliveries addObject:contentPacket];
    }
    
    _mockFeaturedContentPackets = featuredDeliveries;
        
    }
    
    return _mockFeaturedContentPackets;
}

#pragma mark - Menu Items

- (NSArray *)generateEightMockMenuItems{
    
    return [self eightRandomlyOrderedItemsFromMutableArray:self.mockMenuItems];
}

- (NSArray *)mockMenuItems{
    
    if (!_mockMenuItems) {
        
        NSArray *itemNames = @[@"Fresh Squeezed Lemonade",
                               @"Grilled Chicken Sandwich",
                               @"Steak Burrito",
                               @"Veggie Salad",
                               @"Whole-grain 12\" Chicken Sub Sandwich",
                               @"Vanilla Latte",
                               @"Pink Lemonade",
                               @"Grilled Cheese",
                               @"Smokey Salmon",
                               @"Fruit Salad",
                               @"Strawberry Banana Smoothie",
                               @"Linguine Pasta",
                               @"Pork Taco",
                               @"Wheat Shot",
                               @"Energy Bar & OJ",
                               @"Cereal"];
        
        NSArray *ipsumStrings = @[@"Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                                  @"Aenean ac lacus nec.",
                                  @"Fusce vel justo neque. Aenean enim nibh, tempus at mollis et, imperdiet et eros.",
                                  @"Curabitur vel accumsan risus.",
                                  @"Integer eget arcu id turpis sollicitudin hendrerit.",
                                  @"Vivamus varius neque ut lectus lobortis convallis. Phasellus commodo dolor eu metus placerat tempus. Duis pharetra lacus gravida quam venenatis rhoncus."];
        
        NSArray *prices = @[@"549", @"799", @"445", @"859", @"649", @"729", @"629"];
        
        NSMutableArray *menuItems = [NSMutableArray new];
        
        for (NSString * menuItemTitle in itemNames) {
            
            ASMenuItem *menuItem = [ASMenuItem new];
            
            menuItem.itemName = menuItemTitle;
            menuItem.itemDescription = [self randomObjectFromArray:ipsumStrings];
            
            NSString *price = [self randomObjectFromArray:prices];
            menuItem.priceInCents = price.integerValue;
            
            [menuItems addObject:menuItem];
        }
        
        _mockMenuItems = menuItems;
    }

    return _mockMenuItems;
}

#pragma mark - Helpers

- (NSArray *)eightRandomlyOrderedItemsFromMutableArray:(NSMutableArray *)array{
    
    [self shuffleMutableArray:array];
    
    NSMutableArray *items = [NSMutableArray new];
    
    for (int i = 0; i < 8; i ++) {
        
        [items addObject:[array objectAtIndex:i]];
    }
    
    return [NSArray arrayWithArray:items];
}

- (void)shuffleMutableArray:(NSMutableArray *)array{
    
    NSUInteger count = array.count;
    if (count < 1) return;
    
    for (NSUInteger i = 0; i < count - 1; i++) {
        
        NSInteger remainingItems = count - i;
        NSInteger exchangeIndex = i + arc4random_uniform((u_int32_t )remainingItems);
        [array exchangeObjectAtIndex:i withObjectAtIndex:exchangeIndex];
    }
}

- (id)randomObjectFromArray:(NSArray *)array{
    
    NSUInteger index = arc4random() % [array count];
    return [array objectAtIndex:index];
}
                               
@end
