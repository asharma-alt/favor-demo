//
//  ASCategoryBrowser.h
//  Favor Demo
//
//  Created by Amit Sharma on 4/24/16.
//  Copyright © 2016 Amit Sharma. All rights reserved.
//

#import "ASTabbedPageController.h"

@interface ASCategoryBrowser : ASTabbedPageController

- (instancetype)initWithMockData;

@end
