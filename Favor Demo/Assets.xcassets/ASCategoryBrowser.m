//
//  ASCategoryBrowser.m
//  Favor Demo
//
//  Created by Amit Sharma on 4/24/16.
//  Copyright © 2016 Amit Sharma. All rights reserved.
//

#import "ASCategoryBrowser.h"
#import "ASFeaturedTVC.h"
#import "ASMockDataFactory.h"
#import "ASOrderVC.h"
#import "UIColor+favorColors.h"

@interface ASCategoryBrowser () <ASFeaturedTVCDelegate>

@property (strong, nonatomic) UIButton *orderButton;
@property (nonatomic) BOOL currentTableViewIsNearBottom;

@end

@implementation ASCategoryBrowser

#pragma mark - Init

- (instancetype)initWithMockData{
    
    ASMockDataFactory *factory = [[ASMockDataFactory alloc] init];
    
    NSArray *titles = @[@"Italian", @"Snacks", @"Healthy", @"Tex-Mex", @"Salads", @"Deserts", @"Pizzas"];
    NSMutableArray *featuredTVCs = [NSMutableArray new];

    for (int i = 0; i < titles.count; i++) {
        
        NSArray *contentPackets = [factory generateEightMockFeaturedContentPackets];
        
        ASFeaturedTVC *featuredTVC = [[ASFeaturedTVC alloc] initWithFeaturedContentPackets:contentPackets delegate:self];
        
        [featuredTVCs addObject:featuredTVC];
    }
    
    NSDictionary *options = @{kTPCSelectedTextColor : [UIColor favorBlue],
                              kTPCUnselectedTextColor : [UIColor lightGrayColor],
                              kTPCBackgroundColor : [UIColor whiteColor],
                              kTPCFont : [UIFont fontWithName:@"Avenir-Medium" size:18.0f],
                              kTPCSidePadding : [NSNumber numberWithFloat:20.0f],
                              kTPCSpaceBetweenTitles : [NSNumber numberWithFloat:20.0f]};
    
    self = [super initWithViewControllers:[NSArray arrayWithArray:featuredTVCs] titles:titles tabMode:ASTabbedNavigationControllerScrollModeTotalOffset options:options];
    
    return self;
}

#pragma mark - Setup

- (void)viewDidLoad{
    
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setupOrderButton];
}

- (void)setupOrderButton{
    
    _orderButton = [UIButton buttonWithType:UIButtonTypeSystem];
    _orderButton.titleLabel.font = [UIFont fontWithName:@"Avenir-Medium" size:18.0f];
    _orderButton.backgroundColor = [UIColor favorBlue];
    [_orderButton setTitle:@"Order Anything" forState:UIControlStateNormal];
    [_orderButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_orderButton addTarget:self action:@selector(orderAnythingButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    _orderButton.frame = CGRectMake(kOrderAnythingButtonSidePadding,
                                    self.view.frame.size.height - kOrderAnythingButtonBottomPadding - kOrderAnythingButtonHeight,
                                    self.view.frame.size.width - kOrderAnythingButtonSidePadding * 2,
                                    kOrderAnythingButtonHeight);
    
    [self.view addSubview:_orderButton];
    [self.view bringSubviewToFront:_orderButton];
}

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self setupNavigationBar];
}

- (void)setupNavigationBar{
    
    UIImageView *barTitleImage = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 174.0f, 22.0f)];
    barTitleImage.image = [UIImage imageNamed:@"favor_logo_blue"];
    barTitleImage.contentMode = UIViewContentModeScaleAspectFit;
    
    self.navigationItem.titleView = barTitleImage;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    [self.navigationController.navigationBar setTitleVerticalPositionAdjustment:5 forBarMetrics:UIBarMetricsDefault];
}

#pragma mark - Layout

- (void)viewDidLayoutSubviews{
    
    [super viewDidLayoutSubviews];
    [self positionOrderButtonForNearBottom:_currentTableViewIsNearBottom animated:YES];
}

- (void)positionOrderButtonForNearBottom:(BOOL)nearBottom animated:(BOOL)animated{
    
    [self.view bringSubviewToFront:_orderButton];
    
    CGRect frame = CGRectMake(kOrderAnythingButtonSidePadding,
                              self.view.frame.size.height - kOrderAnythingButtonBottomPadding - kOrderAnythingButtonHeight,
                              self.view.frame.size.width - kOrderAnythingButtonSidePadding * 2,
                              kOrderAnythingButtonHeight);
    
    if (nearBottom) frame = CGRectMake(0.0f, self.view.frame.size.height - 60.0f, self.view.frame.size.width, 60.0f);
    
    if (animated) {
        
        [UIView animateWithDuration:0.3f animations:^{
            
            _orderButton.frame = frame;
        }];
    }
    
    else _orderButton.frame = frame;
}

#pragma mark - ASFeaturedTVCDelegate

- (void)presentOrderViewControllerWithFeaturedContent:(ASFeaturedContentPacket *)contentPacket{
    
    ASOrderVC *orderVC = [[ASOrderVC alloc] initWithFeaturedContent:contentPacket];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:orderVC];
    [self presentViewController:navigationController animated:YES completion:nil];
    
    CFRunLoopWakeUp(CFRunLoopGetCurrent()); // workaround for a known bug that is putting the runloop to sleep when it shouldn't be w/ certain UITableView configurations. http://bit.ly/1rCXLVw
}

- (void)tableViewHasScrolledNearBottom:(BOOL)nearBottom{
    
    _currentTableViewIsNearBottom = nearBottom;
    [self.view setNeedsLayout];
}

#pragma mark - Order Anything Button Target

- (void)orderAnythingButtonPressed{
    
    ASOrderVC *orderVC = [[ASOrderVC alloc] init];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:orderVC];
    [self presentViewController:navController animated:YES completion:nil];
}

@end
