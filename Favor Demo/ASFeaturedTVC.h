//
//  ASFeaturedTVC.h
//  Favor Demo
//
//  Created by Amit Sharma on 4/25/16.
//  Copyright © 2016 Amit Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASFeaturedContentPacket.h"

const static CGFloat kOrderAnythingButtonHeight = 60.0f;
const static CGFloat kOrderAnythingButtonSidePadding = 15.0f;
const static CGFloat kOrderAnythingButtonBottomPadding = 15.0f;

@protocol ASFeaturedTVCDelegate <NSObject>

- (void)tableViewHasScrolledNearBottom:(BOOL)nearBottom;
- (void)presentOrderViewControllerWithFeaturedContent:(ASFeaturedContentPacket *)contentPacket;

@end

@interface ASFeaturedTVC : UITableViewController

- (instancetype)initWithFeaturedContentPackets:(NSArray *)featuredContentPackets delegate:(id<ASFeaturedTVCDelegate>)delegate;

@end
