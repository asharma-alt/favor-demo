//
//  ASOrderVC.m
//  Favor Demo
//
//  Created by Amit Sharma on 5/2/16.
//  Copyright © 2016 Amit Sharma. All rights reserved.
//

#import "ASOrderVC.h"
#import "ASLabeledTextFieldView.h"
#import "ASOrderLineItemCell.h"
#import "ASAddNewLineItemCell.h"
#import "ASMenuVC.h"
#import "ASKeyboardStateTracker.h"
#import "ASMenuItemTVC.h"

const static CGFloat kStoreTextFieldHeight = 60.0f;
const static CGFloat kMenuViewControllerHeightToScreenHeightRatio = 0.65f;

@interface ASOrderVC () <UITableViewDelegate, UITableViewDataSource, ASOrderLineItemCellDelegate, ASAddNewLineItemCellDelegate, ASMenuViewControllerDelegate>

@property (strong, nonatomic) ASLabeledTextFieldView *storeTextFieldView;
@property (strong, nonatomic) UITableView *tableView;
@property (nonatomic) BOOL transitionState;

@property (strong, nonatomic) NSMutableArray *lineItems;

@property (strong, nonatomic) ASMenuVC *menuVC;

@property (strong, nonatomic) ASFeaturedContentPacket *featuredContent;

@end

@implementation ASOrderVC

#pragma mark - Init

- (instancetype)initWithFeaturedContent:(ASFeaturedContentPacket *)featuredContent{
    
    self = [super init];
    
    _featuredContent = featuredContent;
    _lineItems = [NSMutableArray arrayWithObject:featuredContent.menuItem.itemName];
    
    return self;
}

#pragma mark - Setup;

- (void)viewDidLoad{
    
    [super viewDidLoad];
    
    [self setupNavigationBar];
    [self setupStoreTextFieldView];
    [self setupTableView];
    [self setupMenuVC];
}

- (void)setupNavigationBar{
    
    self.title = @"Order";
    self.view.backgroundColor = [UIColor colorWithRed:0.965f green:0.973f blue:0.973f alpha:1.00f];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.102f green:0.635f blue:0.863f alpha:1.00f];
    self.navigationController.navigationBar.titleTextAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Avenir-Roman" size:22.0f],
                                                                    NSForegroundColorAttributeName:[UIColor whiteColor]};
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"close_icon"]
                                                                   style:UIBarButtonItemStyleDone
                                                                  target:self
                                                                  action:@selector(dismissSelf)];
    doneButton.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = doneButton;
}

- (void)setupStoreTextFieldView{
    
    _storeTextFieldView = [[ASLabeledTextFieldView alloc]initWithTitle:@"Store Name"];
    _storeTextFieldView.textField.text = _featuredContent.store.name;
    [self.view addSubview:_storeTextFieldView];
}

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    if (!_storeTextFieldView.textField.text || [_storeTextFieldView.textField.text isEqualToString:@""]) [_storeTextFieldView.textField becomeFirstResponder];
}

- (void)setupTableView{
    
    if (!_lineItems) _lineItems = [NSMutableArray new];
    
    CGRect roughInitialFrame = CGRectMake(0.0f, 80.0f, self.view.frame.size.width, self.view.frame.size.height - 80.0f);
    
    _tableView = [[UITableView alloc] initWithFrame:roughInitialFrame style:UITableViewStyleGrouped];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = [UIColor colorWithRed:0.965 green:0.973 blue:0.973 alpha:1.00];
    [_tableView registerClass:[ASOrderLineItemCell class] forCellReuseIdentifier:kOrderLineItemCellReuseIdentifier];
    [_tableView registerClass:[ASAddNewLineItemCell class] forCellReuseIdentifier:kAddNewLineItemCellReuseIdentifier];

    [self.view addSubview:_tableView];
}

- (void)setupMenuVC{
    
    _menuVC = [[ASMenuVC alloc] initWithDelegate:self];
    [self addChildViewController:_menuVC];
    [self.view addSubview:_menuVC.view];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(layoutSubviewForCurrentKeyboardState) name:kLastKnowKeyboardHeightChanged object:nil];
}

#pragma mark - Layout

- (void)viewDidLayoutSubviews{
    
    [super viewDidLayoutSubviews];
    [self layoutSubviewForCurrentKeyboardState];
}

- (void)layoutSubviewForCurrentKeyboardState{
    
    BOOL keyboardVisible = [ASKeyboardStateTracker isKeyboardShowing];
    
    _storeTextFieldView.frame = CGRectMake(0.0f, 0.0f, self.view.frame.size.width, kStoreTextFieldHeight);
    _storeTextFieldView.backgroundColor = [UIColor whiteColor];
    
    CGFloat tableViewHeight = self.view.frame.size.height - kStoreTextFieldHeight;
    if (keyboardVisible) tableViewHeight -= [ASKeyboardStateTracker sharedTracker].lastKnownKeyboardHeight;
    else tableViewHeight -= kMenuViewControllerHeightToScreenHeightRatio * self.view.frame.size.height;
    _tableView.frame = CGRectMake(0.0f, kStoreTextFieldHeight, self.view.frame.size.width, tableViewHeight);
    
    CGFloat menuOriginY = (keyboardVisible) ? self.view.frame.size.height : _tableView.frame.origin.y + _tableView.frame.size.height;
    _menuVC.view.frame = CGRectMake(0.0f, menuOriginY, self.view.frame.size.width, kMenuViewControllerHeightToScreenHeightRatio * self.view.frame.size.height);
}

#pragma mark - Table View Delegate

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
 
    if (indexPath.row == _lineItems.count) {
        
        ASAddNewLineItemCell *cell = [tableView dequeueReusableCellWithIdentifier:kAddNewLineItemCellReuseIdentifier];
        [cell prepareForReuseWithDelegate:self];
        
        return cell;
    }
    
    ASOrderLineItemCell *cell = [tableView dequeueReusableCellWithIdentifier:kOrderLineItemCellReuseIdentifier];
    [cell prepareForReuseWithText:[_lineItems objectAtIndex:indexPath.row] index:indexPath.row delegate:self];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _lineItems.count + 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 0.1f;
}

#pragma mark - ASAddNewLineItemCellDelegate

- (void)addNewLineItem{
    
    [_lineItems addObject:@""];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_lineItems.count - 1 inSection:0];
    [_tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationMiddle];
    
    ASOrderLineItemCell *cell = [_tableView cellForRowAtIndexPath:indexPath];
    [cell makeTextFieldFirstResponder];
}

#pragma mark - ASOrderLineItemCellDelegate

- (void)removeLineItemAtIndex:(NSInteger)index{
    
    if (_lineItems.count - 1 < index) return; // should never evaluate to true
    
    [_lineItems removeObjectAtIndex:index];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    [_tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationMiddle];
    [self reassignCellIndexs];
}

- (void)updatedLineItemText:(NSString *)text index:(NSInteger)index{
    
    if (_lineItems.count - 1 < index) return; // should never evaluate to true
    
    [_lineItems replaceObjectAtIndex:index withObject:text];
}

- (void)showMenu{
    
    [self resignLineItemCellsFirstResponders];
}

#pragma mark - ASMenuViewControllerDelegate

- (void)addMenuItemToOrder:(NSString *)menuItem{
    
    [_lineItems addObject:menuItem];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_lineItems.count - 1 inSection:0];
    [_tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationMiddle];
}

#pragma mark - View Will Disappear

- (void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    [_storeTextFieldView.textField resignFirstResponder];
    [self resignLineItemCellsFirstResponders];
}

#pragma mark - Other

- (void)reassignCellIndexs{
    
    for (ASOrderLineItemCell *cell in [_tableView visibleCells]) {
        
        if ([cell respondsToSelector:@selector(setLineItemIndex:)]) {
            
            cell.lineItemIndex = [_tableView indexPathForCell:cell].row;
        }
    }
}

- (void)dismissSelf{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)resignLineItemCellsFirstResponders{
    
    for (ASOrderLineItemCell *cell in [_tableView visibleCells]) {
        
        if ([cell respondsToSelector:@selector(resignTextField)]) {
            
            [cell resignTextField];
        }
    }
}

@end
