//
//  ASMenuItemTVC.m
//  Favor Demo
//
//  Created by Amit Sharma on 5/3/16.
//  Copyright © 2016 Amit Sharma. All rights reserved.
//

#import "ASMenuItemTVC.h"
#import "ASMenuItemCell.h"

@interface ASMenuItemTVC ()

@property (strong, nonatomic) NSArray *menuItems;
@property (weak, nonatomic) id<ASMenuTableViewControllerDelegate> delegate;

@end

@implementation ASMenuItemTVC

#pragma mark - Init;

- (instancetype)initWithDelegate:(id<ASMenuTableViewControllerDelegate>)delegate menuItems:(NSArray *)menuItems{
    
    self = [super init];

    _delegate = delegate;
    _menuItems = menuItems;
    
    return self;
}

#pragma mark - Setup

- (void)viewDidLoad{
    
    [super viewDidLoad];
    [self setupTableView];
}

- (void)setupTableView{
    
    self.tableView.estimatedRowHeight = 100.0f;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ASMenuItemCell" bundle:nil] forCellReuseIdentifier:@"menu_item_cell"];
}

#pragma mark - UITableViewDelegate / UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ASMenuItemCell *cell = [tableView dequeueReusableCellWithIdentifier:@"menu_item_cell"];
    
    ASMenuItem *item = [_menuItems objectAtIndex:indexPath.row];
    
    [cell prepareForReuseWithPacket:item];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _menuItems.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ASMenuItem *item = [_menuItems objectAtIndex:indexPath.row];
    NSString *menuItemTitle = [NSString stringWithFormat:@"1x %@", item.itemName];
    
    [_delegate menuItemWasSelected:menuItemTitle];
}

#pragma mark - Top Menu Item Offset

- (void)updateTableViewTopContentInset:(CGFloat)topInset{
    
    self.tableView.contentInset = UIEdgeInsetsMake(topInset, 0.0f, 0.0f, 0.0f);
    self.tableView.scrollIndicatorInsets = (UIEdgeInsetsMake(topInset, 0.0f, 0.0f, 0.0f));
    self.tableView.contentOffset = CGPointMake(0.0f, - topInset);
}

@end
