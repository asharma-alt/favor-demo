//
//  ASTabScrollerViewModes.h
//  Favor Demo
//
//  Created by Amit Sharma on 5/5/16.
//  Copyright © 2016 Amit Sharma. All rights reserved.
//

static NSString * kTPCSpaceBetweenTitles = @"tcp_title_spacing";
static NSString * kTPCSidePadding = @"tcp_side_padding";
static NSString * kTPCFont = @"tcp_font";

static NSString * kTPCSelectedTextColor = @"tcp_selected_text_color";
static NSString * kTPCUnselectedTextColor = @"tcp_unselected_text_color";
static NSString * kTPCBackgroundColor = @"tcp_background_color";

typedef enum : NSUInteger {
    ASTabbedNavigationControllerScrollModeAlignLeft,
    ASTabbedNavigationControllerScrollModeTotalOffset,
    ASTabbedNavigationControllerScrollModeCenter
} ASTabbedNavigationControllerScrollMode;

