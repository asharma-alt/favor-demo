//
//  ASTabScrollerView.h
//  Amit Sharma
//
//  Created by Amit Sharma on 04/28/15.
//  Copyright © 2015 Amit Sharma. All rights reserved.
//

#import "ASTabScrollerView.h"
#import "UIColor+favorColors.h"

const static CGFloat kCarrotViewHeight = 10.0f;
const static CGFloat kExtraHeightRequiredForCarrot = 15.0f;

const static CGFloat kDefaultSidePadding = 10.0f;
const static CGFloat kDefaultSpaceBetweenTitles = 20.0f;
const static CGFloat kDefaultFontSize = 22.0f;

@interface ASTabScrollerView () <UIScrollViewDelegate>

@property (strong, nonatomic) NSDictionary *options;
@property (strong, nonatomic, readwrite) NSArray *titles;
@property (nonatomic) ASTabbedNavigationControllerScrollMode tabMode;

@property (strong, nonatomic) NSArray *buttons;
@property (strong, nonatomic) UIScrollView *scrollView;

@property (nonatomic) BOOL animationLock;
@property (nonatomic) CGFloat maxContentOffsetX;
@property (nonatomic, readwrite) NSInteger selectedIndex;

// centering mode only
@property (nonatomic) CGFloat offsetToCenterPreviousButton;
@property (nonatomic) CGFloat offsetToCenterCurrentButton;
@property (nonatomic) CGFloat offsetToCenterNextButton;

@end

@implementation ASTabScrollerView

- (instancetype)initWithTabMode:(ASTabbedNavigationControllerScrollMode)tabMode titles:(NSArray *)titles options:(NSDictionary *)options{
    
    if (!titles || titles.count == 0) return nil;
    
    self = [super init];
    
    _tabMode = tabMode;
    _titles = titles;
    _options = options;
    
    [self setupScrollView];
    [self setupButtons];
    [self layoutTitleButtonsAndUpdateContentSize];
    [self selectButton:[_buttons firstObject]];
    
    return self;
}

#pragma mark - Setup

- (void)setupScrollView{
    
    _scrollView = [[UIScrollView alloc]init];
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.delegate = self;
    _scrollView.backgroundColor = [self scrollBarBackgroundColor];
    
    [self addSubview:_scrollView];
}

- (void)setupButtons{
    
    NSMutableArray *tempButtons = [[NSMutableArray alloc] init];
    CGFloat tallestTitleLabelHeight = 0.0f;
    
    for (NSString *title in _titles) {
        
        UIButton *newButton = [[UIButton alloc] init];
        
        [newButton.titleLabel setFont:[self titleFont]];
        [newButton setTitleColor:[UIColor favorBlue] forState:UIControlStateNormal];
        [newButton setTitle:title forState:UIControlStateNormal];
        [newButton addTarget:self action:@selector(titleButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        CGFloat titleLabelHeight = [newButton.titleLabel sizeThatFits:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)].height;
        tallestTitleLabelHeight = MAX(titleLabelHeight, tallestTitleLabelHeight);
        
        [tempButtons addObject:newButton];
        [_scrollView addSubview:newButton];
    }
    
    if (_tabMode == ASTabbedNavigationControllerScrollModeCenter) _suggestedMinimumHeight = tallestTitleLabelHeight + kExtraHeightRequiredForCarrot;
    else _suggestedMinimumHeight = tallestTitleLabelHeight;
    
    _buttons = [NSArray arrayWithArray:tempButtons];
}

#pragma mark - Layout Management

- (void)layoutSubviews{
    
    [super layoutSubviews];
    
    if (_tabMode == ASTabbedNavigationControllerScrollModeCenter) {
        
        _scrollView.contentInset = UIEdgeInsetsZero;
        _scrollView.frame = CGRectMake(0.0f, 0.0f, self.frame.size.width, self.frame.size.height);
        [self addTriangleMarkerMaskToView:self];
    }
    
    else{
        
        _scrollView.contentInset = UIEdgeInsetsZero;
        _scrollView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    }
    
    [self layoutTitleButtonsAndUpdateContentSize];
}

- (void)layoutTitleButtonsAndUpdateContentSize{
    
    BOOL isCentered = (_tabMode == ASTabbedNavigationControllerScrollModeCenter);
    
    [_buttons makeObjectsPerformSelector:@selector(sizeToFit)];
    
    CGFloat buttonOffsetX = [self sidePadding];
    
    if (isCentered) {
        
        UIButton *firstButton = [_buttons firstObject];
        buttonOffsetX = (self.frame.size.width - firstButton.frame.size.width) / 2;
    }
    
    for (UIButton *button in _buttons) {
        
        button.frame = CGRectMake(buttonOffsetX, 0.0, button.frame.size.width, _scrollView.frame.size.height);
        buttonOffsetX += button.frame.size.width + [self spaceBetweenTitles];
    }
    
    CGFloat totalWidth = buttonOffsetX + ([self sidePadding] - [self spaceBetweenTitles]);
    
    if (isCentered){
        
        UIButton *lastButton = [_buttons lastObject];
        totalWidth = buttonOffsetX + (self.frame.size.width - lastButton.frame.size.width) / 2;
    }
    
    _scrollView.contentSize = CGSizeMake(totalWidth, _scrollView.frame.size.height);
    _maxContentOffsetX = _scrollView.contentSize.width - _scrollView.frame.size.width;
}

#pragma mark - Mask Management

- (void)addTriangleMarkerMaskToView:(UIView *)view{
    
    CGFloat triangleBaselineWidth = 15.0f;
    CGFloat triangleHeight = kCarrotViewHeight;
    
    UIBezierPath* trianglePath = [UIBezierPath bezierPath];
    [trianglePath moveToPoint:CGPointMake((view.frame.size.width - triangleBaselineWidth) / 2, view.frame.size.height)];
    [trianglePath addLineToPoint:CGPointMake((view.frame.size.width) / 2, view.frame.size.height- triangleHeight)];
    [trianglePath addLineToPoint:CGPointMake((view.frame.size.width + triangleBaselineWidth) / 2, view.frame.size.height)];
    [trianglePath closePath];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    CGMutablePathRef maskPath = CGPathCreateMutable();
    CGPathAddRect(maskPath, NULL, CGRectMake(0.0f, 0.0f, view.frame.size.width, view.frame.size.height));
    CGPathAddPath(maskPath, nil, trianglePath.CGPath);
    [maskLayer setPath:maskPath];
    maskLayer.fillRule = kCAFillRuleEvenOdd;
    
    view.layer.mask = maskLayer;
}

#pragma mark - Button Selection Management

- (void)selectButtonAtIndex:(NSInteger)index{
    
    if (index > _buttons.count) return; // should never be true
    [self selectButton:[_buttons objectAtIndex:index]];
}

- (void)selectButton:(UIButton *)selectedButton{
    
    _selectedIndex = [_buttons indexOfObject:selectedButton];
    
    for (UIButton *button in _buttons) {
        
        UIColor *textColor = (button == selectedButton) ? [self selectedButtonColor] : [self unselectedButtonColor];
        [button setTitleColor:textColor forState:UIControlStateNormal];
    }
    
    if (_tabMode == ASTabbedNavigationControllerScrollModeAlignLeft) [self scrollToLeftAlignButtonAtIndex:_selectedIndex];
    if (_tabMode == ASTabbedNavigationControllerScrollModeCenter) [self scrollToCenterButtonAtIndex:_selectedIndex];
}

- (void)titleButtonPressed:(UIButton *)button{
    
    _animationLock = YES;
    
    if (_tabMode == ASTabbedNavigationControllerScrollModeTotalOffset) {
        
        CGFloat percentageOfMaxOffset = [_buttons indexOfObject:button] / (_buttons.count - 1.0);
        CGFloat contentOffsetX = MIN(_maxContentOffsetX * percentageOfMaxOffset, _maxContentOffsetX);
        
        [UIView animateWithDuration:0.3f animations:^{
            
            _scrollView.contentOffset = CGPointMake(contentOffsetX, 0.0f);
        }];
    }
    
    [self.delegate selectViewControllerAtIndex:[_buttons indexOfObject:button] completion:^{
        
        _animationLock = NO;
    }];
    
    [self selectButton:button];
}

#pragma mark - Scroll To Percentage Offset (Total Mode)

- (void)scrollToPercentageOfMaxContentOffset:(CGFloat)percentage{
    
    if (_animationLock || _tabMode == ASTabbedNavigationControllerScrollModeAlignLeft || _tabMode == ASTabbedNavigationControllerScrollModeCenter) return;
    
    if (_tabMode == ASTabbedNavigationControllerScrollModeTotalOffset) {
        
        CGFloat contentOffsetX = _maxContentOffsetX * percentage;
        CGPoint contentOffset = (percentage > 1.0) ? CGPointMake(contentOffsetX, 0.0f) : CGPointMake(MIN(_maxContentOffsetX, contentOffsetX), 0.0f);
        _scrollView.contentOffset = contentOffset;
    }
}

#pragma mark - Left Align Selected Button (Left Mode)

- (void)scrollToLeftAlignButtonAtIndex:(NSInteger)index{
    
    UIButton *button = (UIButton *)[_buttons objectAtIndex:index];
    CGFloat snapToContentOffsetX = button.frame.origin.x - [self sidePadding];
    
    CGFloat maxContentOffsetX = _scrollView.contentSize.width - _scrollView.frame.size.width;
    snapToContentOffsetX = MIN(maxContentOffsetX, snapToContentOffsetX);
    
    [_scrollView setContentOffset:CGPointMake(snapToContentOffsetX, 0) animated:YES];
    
    _selectedIndex = index;
}

#pragma mark - Scroll To Center Button (Centering Mode)

- (void)scrollToCenterButtonAtIndex:(NSInteger)index{
    
    UIButton *button = (UIButton *)[_buttons objectAtIndex:index];
    CGFloat snapToContentOffsetX = button.frame.origin.x - (_scrollView.frame.size.width - button.frame.size.width) / 2;
    [_scrollView setContentOffset:CGPointMake(snapToContentOffsetX, 0) animated:YES];
    
    _selectedIndex = index;
    
    [self assignCenteringContentOffsets];
}

#pragma mark - Scroll To Percentage To Next Button (Centering Mode)

- (void)scrollToPercentageToNextButton:(CGFloat)percentage{
    
    if (_animationLock || _tabMode == ASTabbedNavigationControllerScrollModeAlignLeft || _tabMode == ASTabbedNavigationControllerScrollModeTotalOffset) return;
    
    if (percentage > 0.0f) {
        
        CGFloat additionalXOffset = _offsetToCenterNextButton - _offsetToCenterCurrentButton;
        _scrollView.contentOffset = CGPointMake(_offsetToCenterCurrentButton + additionalXOffset * percentage, 0.0f);
    }
    
    else {
        
        CGFloat additionalXOffset = _offsetToCenterCurrentButton - _offsetToCenterPreviousButton;
        _scrollView.contentOffset = CGPointMake(_offsetToCenterCurrentButton + additionalXOffset * percentage, 0.0f);
    }
}

- (void)assignCenteringContentOffsets{
    
    UIButton *currentButton = (UIButton *)[_buttons objectAtIndex:_selectedIndex];
    _offsetToCenterCurrentButton = currentButton.frame.origin.x - (_scrollView.frame.size.width - currentButton.frame.size.width) / 2;
    
    if (_selectedIndex > 0) {
        
        UIButton *previousButton = [_buttons objectAtIndex:_selectedIndex - 1];
        _offsetToCenterPreviousButton = previousButton.frame.origin.x - (_scrollView.frame.size.width - previousButton.frame.size.width) / 2;
    }
    
    else _offsetToCenterPreviousButton = -30.0f;
    
    if (_selectedIndex < _buttons.count - 1) {
        
        UIButton *nextButton = [_buttons objectAtIndex:_selectedIndex + 1];
        _offsetToCenterNextButton = nextButton.frame.origin.x - (_scrollView.frame.size.width - nextButton.frame.size.width) / 2;
    }
    
    else _offsetToCenterNextButton = _scrollView.contentSize.width + 30.0f;
}

#pragma mark - Option Dictionary Helpers

- (CGFloat)spaceBetweenTitles{
    
    NSNumber *spaceBetweenTitles = [_options valueForKey:kTPCSpaceBetweenTitles];
    if (!spaceBetweenTitles) return kDefaultSpaceBetweenTitles;
    
    return spaceBetweenTitles.floatValue;
}

- (CGFloat)sidePadding{
    
    NSNumber *sidePadding = [_options valueForKey:kTPCSidePadding];
    if (!sidePadding) return kDefaultSidePadding;
    
    return sidePadding.floatValue;
}

- (UIFont *)titleFont{
    
    UIFont *font = [_options valueForKey:kTPCFont];
    if (!font) return [UIFont systemFontOfSize:kDefaultFontSize];

    return font;
}

- (UIColor *)selectedButtonColor{
    
    UIColor *color = [_options valueForKey:kTPCSelectedTextColor];
    if (!color) return [UIColor blackColor];
    
    return color;
}

- (UIColor *)unselectedButtonColor{
    
    UIColor *color = [_options valueForKey:kTPCUnselectedTextColor];
    if (!color) return [UIColor lightGrayColor];
    
    return color;
}

- (UIColor *)scrollBarBackgroundColor{
    
    UIColor *color = [_options valueForKey:kTPCBackgroundColor];
    if (!color) return [UIColor whiteColor];
    
    return color;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    if (_tabMode != ASTabbedNavigationControllerScrollModeCenter) return;
    [self snapToCenterMostButton];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    if (_tabMode != ASTabbedNavigationControllerScrollModeCenter || decelerate) return;
    else [self snapToCenterMostButton];
}

- (void)snapToCenterMostButton{
    
    CGFloat contentOffsetX = _scrollView.contentOffset.x;
    CGFloat smallestDistanceToOffsetButton = CGFLOAT_MAX;
    NSInteger buttonIndexOfSmallestDistanceToButtonCenter = 0;
    NSInteger buttonIndex = 0;
    
    for (UIButton *button in _buttons) {
        
        CGFloat distanceToCenterButton = fabs((contentOffsetX + _scrollView.frame.size.width / 2) - (button.frame.origin.x + button.frame.size.width / 2));
        
        if (distanceToCenterButton < smallestDistanceToOffsetButton) {
            
            smallestDistanceToOffsetButton =  distanceToCenterButton;
            buttonIndexOfSmallestDistanceToButtonCenter = buttonIndex;
        }
        
        buttonIndex ++;
    }
    
    [self titleButtonPressed:[_buttons objectAtIndex:buttonIndexOfSmallestDistanceToButtonCenter]];
}

@end
