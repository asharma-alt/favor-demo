//
//  UIColor+favorColors.h
//  Favor Demo
//
//  Created by Amit Sharma on 5/9/16.
//  Copyright © 2016 Amit Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (favorColors)

+ (UIColor *)favorBlue;

@end
