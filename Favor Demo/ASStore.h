//
//  ASStore.h
//  Favor Demo
//
//  Created by Amit Sharma on 5/8/16.
//  Copyright © 2016 Amit Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ASStore : NSObject

@property (strong, nonatomic) NSString *name;

@end
