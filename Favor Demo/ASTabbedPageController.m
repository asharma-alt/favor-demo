//
//  ASTabbedPageController.m
//  Favor Demo
//
//  Created by Amit Sharma on 4/24/16.
//  Copyright © 2016 Amit Sharma. All rights reserved.
//

#import "ASTabbedPageController.h"
#import "ASTabScrollerView.h"
#import "ASOrderVC.h"

@interface ASTabbedPageController () <UIPageViewControllerDelegate, UIPageViewControllerDataSource, UIScrollViewDelegate, ASTabScrollerDelegate>

@property (strong, nonatomic) ASTabScrollerView *tabScroller;

@property (strong, nonatomic) NSArray *controllers;
@property (strong, nonatomic) NSArray *titles;
@property (nonatomic) ASTabbedNavigationControllerScrollMode tabMode;

@property (strong, nonatomic) UIPageViewController *pageController;
@property (nonatomic) NSInteger currentIndex;

@property (nonatomic) CGFloat maxContentOffset;
@property (nonatomic) CGFloat currentPageOffset;

@property (nonatomic, readwrite) CGFloat centeredModeTopContentInset;

@end

@implementation ASTabbedPageController

// **** NOTE TO FAVOR ENGINEERS **** //
//
// I utilized a traditional setup for the sake of simplicity in this demo,
// but it appears that the "UIPageViewControllers" (both the home VC and menu from the order VC) in the real Favor app may not actually be UIPageViewControllers.
// I may be incorrect, but it appears that the tableviews are directly laid-out onto a UIScrollView instead in a page view controller.
// I understand that it's probably a performance decision. I just wanted to clarify that the difference.

#pragma mark - Init

- (instancetype)initWithViewControllers:(NSArray *)controllers titles:(NSArray *)titles tabMode:(ASTabbedNavigationControllerScrollMode)tabMode options:(NSDictionary *)options{
    
    self = [super init];

    _controllers = controllers;
    _titles = titles;
    _tabMode = tabMode;

    [self setupScrollBarWithTabMode:tabMode titles:titles options:options];
    [self setupPageViewController];
    [self updateCurrentViewController];
    
    return self;
}

#pragma mark - Setup

- (void)setupScrollBarWithTabMode:(ASTabbedNavigationControllerScrollMode)tabMode titles:(NSArray *)titles options:(NSDictionary *)options{
    
    _tabScroller = [[ASTabScrollerView alloc] initWithTabMode:tabMode titles:titles options:options];
    _tabScroller.delegate = self;
    [self.view addSubview:_tabScroller];
}

- (void)setupPageViewController{
    
    _pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:0];
    _pageController.delegate = self;
    _pageController.dataSource = self;
    
    [_pageController setViewControllers:@[[_controllers firstObject]] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    for (UIView *view in self.pageController.view.subviews) {
        
        if ([view isKindOfClass:[UIScrollView class]]) {
            
            [(UIScrollView *)view setDelegate:self];
            
        }
    }
    
    [self.view addSubview:_pageController.view];
    [self addChildViewController:_pageController];
}

#pragma mark - Layout

- (void)viewDidLayoutSubviews{
    
    [super viewDidLayoutSubviews];
    
    [self updateMaxContentOffset];
    [self.view bringSubviewToFront:_tabScroller];
    
    _tabScroller.frame = CGRectMake(0.0f, 0.0f, self.view.frame.size.width, _tabScroller.suggestedMinimumHeight + 10.0f); // + 10.0f for extra padding
    
    BOOL isCenteredMode = (_tabMode == ASTabbedNavigationControllerScrollModeCenter);
    
    CGFloat pageControllerOriginY = (isCenteredMode) ? 0.0f : _tabScroller.frame.origin.y + _tabScroller.frame.size.height;
    CGFloat pageControllerHeight = (isCenteredMode) ? self.view.frame.size.height : self.view.frame.size.height - _tabScroller.frame.size.height;

    _pageController.view.frame =  CGRectMake(0.0f, pageControllerOriginY, self.view.frame.size.width, pageControllerHeight);
    
    _centeredModeTopContentInset = (isCenteredMode) ? _tabScroller.frame.origin.y + _tabScroller.frame.size.height : 0.0f;
}

- (void)updateMaxContentOffset{
    
    _maxContentOffset = self.view.frame.size.width * (_controllers.count -1);
}

#pragma mark - UIPageViewController Data Source

-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController{
    
    if (_currentIndex == _controllers.count - 1) return nil;
    
    NSInteger index = [self indexOfPageViewController:viewController];
    
    if (index == NSNotFound) return nil;
    index ++;
    if (index == _controllers.count) index = 0;
    
    return [_controllers objectAtIndex:index];
}

-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController{
    
    if (_currentIndex == 0) return nil;
    
    NSInteger index = [self indexOfPageViewController:viewController];
    
    if (index == NSNotFound) return nil;
    if (index == 0) index = _controllers.count - 1;
    else index --;
    
    return [_controllers objectAtIndex:index];
}

#pragma mark - UIPageViewController Delegate

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed{
    
    if (completed) {
        
        [self updateCurrentViewController];
        [_tabScroller selectButtonAtIndex:_currentIndex];
    }
}

#pragma mark - Page View Controller Helpers

- (void)updateCurrentViewController{
    
    UIViewController *currentController = self.pageController.viewControllers.firstObject;
    _currentIndex = [self indexOfPageViewController:currentController];
    _currentPageOffset = (_currentIndex - 1) * self.view.frame.size.width;
}

- (NSInteger)indexOfPageViewController:(UIViewController*)page{
    
    return [_controllers indexOfObject:page];
}

#pragma mark - ASTabScrollerDelegate

- (void)selectViewControllerAtIndex:(NSInteger)index completion:(ASAnimationCompletion)completion{
    
    UIPageViewControllerNavigationDirection direction = (index > _currentIndex) ? UIPageViewControllerNavigationDirectionForward : UIPageViewControllerNavigationDirectionReverse;
    
    __weak ASTabbedPageController *weakSelf = self;
    
    [_pageController setViewControllers:@[[_controllers objectAtIndex:index]] direction:direction animated:YES completion:^(BOOL finished) {
        
        [weakSelf updateCurrentViewController];
        completion();
    }];
}

#pragma mark - Scroll View Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    CGFloat currentOffset = _currentPageOffset + scrollView.contentOffset.x;
    CGFloat percentOfMaxContentOffsetScrolled = currentOffset / _maxContentOffset;
    [_tabScroller scrollToPercentageOfMaxContentOffset:percentOfMaxContentOffsetScrolled];
    
    [_tabScroller scrollToPercentageToNextButton:(scrollView.contentOffset.x / self.view.frame.size.width) -1];
    
    // NSLog(@"CW: %f, CO: %f, COP %f, POMCS %f", scrollView.contentSize.width, scrollView.contentOffset.x, scrollView.contentOffset.x / scrollView.contentSize.width, percentOfMaxContentOffsetScrolled); // Debug
}

@end
