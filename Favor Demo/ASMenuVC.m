//
//  ASMenuVC.m
//  Favor Demo
//
//  Created by Amit Sharma on 5/3/16.
//  Copyright © 2016 Amit Sharma. All rights reserved.
//

#import "ASMenuVC.h"
#import "ASTabScrollerView.h"
#import "ASMenuItemTVC.h"
#import "ASMenuItemCell.h"
#import "ASMockDataFactory.h"

@interface ASMenuVC () <ASMenuTableViewControllerDelegate>

@property (weak, nonatomic) id<ASMenuViewControllerDelegate> delegate;
@property (strong, nonatomic) NSArray *menuTVCs;

@end

@implementation ASMenuVC

- (instancetype)initWithDelegate:(id<ASMenuViewControllerDelegate>)delegate{

    NSArray *titles = @[@"Appetizers", @"Sandwiches", @"Salads", @"Pastas", @"Seafood", @"Deserts"];
    
    ASMockDataFactory *mockDataFactory = [[ASMockDataFactory alloc] init];
    
    NSMutableArray *controllers = [NSMutableArray new];
        
    for (int i = 0; i < titles.count; i ++) {
        
        NSArray *menuItems = [mockDataFactory generateEightMockMenuItems];
        
        ASMenuItemTVC *menuItemTVC = [[ASMenuItemTVC alloc] initWithDelegate:self menuItems:menuItems];
        
        [controllers addObject:menuItemTVC];
    }
    
    _menuTVCs = [NSArray arrayWithArray:controllers];
    
    NSDictionary *options = @{
                              kTPCSelectedTextColor : [UIColor whiteColor],
                              kTPCUnselectedTextColor : [UIColor colorWithRed:0.855 green:0.878 blue:0.890 alpha:1.00],
                              kTPCBackgroundColor : [UIColor colorWithRed:0.702 green:0.753 blue:0.776 alpha:1.00],
                              kTPCFont : [UIFont fontWithName:@"Avenir-Light" size:18.0f],
                              kTPCSidePadding : [NSNumber numberWithFloat:20.0f],
                              kTPCSpaceBetweenTitles : [NSNumber numberWithFloat:20.0f]
                              };
    
    self = [super initWithViewControllers:[NSArray arrayWithArray:controllers] titles:titles tabMode:ASTabbedNavigationControllerScrollModeCenter options:options];

    _delegate = delegate;

    return self;
}

- (void)viewDidLayoutSubviews{
    
    [super viewDidLayoutSubviews];
    
    for (ASMenuItemTVC *menuItemTVC in _menuTVCs) {
        
        if ([menuItemTVC respondsToSelector:@selector(updateTableViewTopContentInset:)]) {
            
            [menuItemTVC updateTableViewTopContentInset:self.centeredModeTopContentInset];
        }
    }
}

#pragma mark - ASMenuTableViewControllerDelegate

- (void)menuItemWasSelected:(NSString *)menuItem{
    
    [self addMenuItemToOrder:menuItem];
}

- (void)addMenuItemToOrder:(NSString *)menuItem{
    
    [_delegate addMenuItemToOrder:menuItem];
}

@end
