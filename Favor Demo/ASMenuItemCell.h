//
//  ASMenuItemCell.h
//  Favor Demo
//
//  Created by Amit Sharma on 5/7/16.
//  Copyright © 2016 Amit Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASMenuItem.h"

@interface ASMenuItemCell : UITableViewCell

- (void)prepareForReuseWithPacket:(ASMenuItem *)menuItem;

@end
