//
//  ASLabeledTextFieldView.h
//  Favor Demo
//
//  Created by Amit Sharma on 5/2/16.
//  Copyright © 2016 Amit Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ASLabeledTextFieldView : UIView

@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UITextField *textField;
@property (strong, nonatomic) UIButton *clearButton;

- (instancetype)initWithTitle:(NSString *)title;

@end
