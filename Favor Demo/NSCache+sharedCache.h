//
//  NSCache+sharedCache.h
//  Favor Demo
//
//  Created by Amit Sharma on 4/25/16.
//  Copyright © 2016 Amit Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSCache (sharedCache)

+ (NSCache*)sharedCache;

@end
