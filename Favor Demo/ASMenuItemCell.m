//
//  ASMenuItemCell.m
//  Favor Demo
//
//  Created by Amit Sharma on 5/7/16.
//  Copyright © 2016 Amit Sharma. All rights reserved.
//

#import "ASMenuItemCell.h"

@interface ASMenuItemCell ()

@property (weak, nonatomic) IBOutlet UILabel *itemLabel;
@property (weak, nonatomic) IBOutlet UILabel *itemDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@end

@implementation ASMenuItemCell

- (void)prepareForReuseWithPacket:(ASMenuItem *)menuItem{
    
    _itemLabel.text = menuItem.itemName;
    _itemDescriptionLabel.text = menuItem.itemDescription;
    _priceLabel.text = [NSString stringWithFormat:@"$%.2f", menuItem.priceInCents / 100.0f];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated{
    
    [super setSelected:selected animated:animated];
    if (selected) [self setSelected:NO animated:YES];
}


@end
