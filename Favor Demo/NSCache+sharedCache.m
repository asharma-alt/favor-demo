//
//  NSCache+sharedCache.m
//  Favor Demo
//
//  Created by Amit Sharma on 4/25/16.
//  Copyright © 2016 Amit Sharma. All rights reserved.
//

#import "NSCache+sharedCache.h"

@implementation NSCache (sharedCache)

+ (NSCache *)sharedCache{
    
    static dispatch_once_t predicate = 0;
    __strong static NSCache *globalCache = nil;
    
    dispatch_once(&predicate, ^{
        
        globalCache = [[self alloc] init];
    });
    
    return globalCache;
}

@end
