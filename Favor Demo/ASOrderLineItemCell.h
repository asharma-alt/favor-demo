//
//  ASOrderLineItemCell.h
//  Favor Demo
//
//  Created by Amit Sharma on 5/2/16.
//  Copyright © 2016 Amit Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString * kOrderLineItemCellReuseIdentifier = @"line_item_cell";

@protocol ASOrderLineItemCellDelegate <NSObject>

- (void)updatedLineItemText:(NSString *)text index:(NSInteger)index;
- (void)removeLineItemAtIndex:(NSInteger)index;
- (void)showMenu;

@end

@interface ASOrderLineItemCell : UITableViewCell

@property (nonatomic) NSInteger lineItemIndex;

- (void)prepareForReuseWithText:(NSString *)text index:(NSInteger)index delegate:(id<ASOrderLineItemCellDelegate>)delegate;
- (NSString *)lineItemValue;
- (void)resignTextField;
- (void)makeTextFieldFirstResponder;

@end
