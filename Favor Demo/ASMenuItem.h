//
//  ASMenuItem.h
//  Favor Demo
//
//  Created by Amit Sharma on 5/8/16.
//  Copyright © 2016 Amit Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ASMenuItem : NSObject

@property (strong, nonatomic) NSString *itemName;
@property (strong, nonatomic) NSString *itemDescription;
@property (nonatomic) NSInteger priceInCents;

@end
