//
//  ASKeyboardStateTracker.m
//  Favor Demo
//
//  Created by Amit Sharma on 5/3/16.
//  Copyright © 2016 Amit Sharma. All rights reserved.
//

#import "ASKeyboardStateTracker.h"

@interface ASKeyboardStateTracker ()

@property (nonatomic, readwrite) CGFloat lastKnownKeyboardHeight;

@end

@implementation ASKeyboardStateTracker

+ (instancetype)sharedTracker{
    
    static ASKeyboardStateTracker *sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        sharedInstance = [[ASKeyboardStateTracker alloc] init];
        [sharedInstance registerForKeyboardActivityNotifications];
    });
    
    return sharedInstance;
}

#pragma mark - Keyboard Management

- (void)registerForKeyboardActivityNotifications{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleKeyboardActivityNotification:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleKeyboardActivityNotification:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleKeyboardActivityNotification:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleKeyboardActivityNotification:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
}

- (void)handleKeyboardActivityNotification:(NSNotification *)notification{
    
    CGFloat previouslyLastKnowKeyboardHeight = _lastKnownKeyboardHeight;
    
    if (notification.name == UIKeyboardWillHideNotification || notification.name == UIKeyboardDidHideNotification) {
        
        _lastKnownKeyboardHeight = 0.0f;
    }
    
    else {
        
        NSValue *keyboardHeight = [[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
        _lastKnownKeyboardHeight = [keyboardHeight CGRectValue].size.height;
    }
    
    if (previouslyLastKnowKeyboardHeight != _lastKnownKeyboardHeight) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kLastKnowKeyboardHeightChanged object:nil];
    }
}

+ (BOOL)isKeyboardShowing{
    
    if ([ASKeyboardStateTracker sharedTracker].lastKnownKeyboardHeight > 0.0f) return YES;
    return NO;
}

@end
