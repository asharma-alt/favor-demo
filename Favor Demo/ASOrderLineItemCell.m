//
//  ASOrderLineItemCell.m
//  Favor Demo
//
//  Created by Amit Sharma on 5/2/16.
//  Copyright © 2016 Amit Sharma. All rights reserved.
//

#import "ASOrderLineItemCell.h"
#import "UIColor+favorColors.h"

const static CGFloat kLeftTextFieldPadding = 15.0f;
const static CGFloat kMiddleTextFieldPadding = 10.0f;
const static CGFloat kRightTextFieldPadding = 5.0f;
const static CGFloat kCloseButtonWidth = 30.0f;

const static CGFloat kViewMenuButtonHeight = 45.0f;

@interface ASOrderLineItemCell () <UITextFieldDelegate>

@property (strong, nonatomic) UITextField *textField;
@property (strong, nonatomic) UIButton *removeButton;
@property (strong, nonatomic) CAShapeLayer *dottedLineLayer;
@property (weak, nonatomic) id<ASOrderLineItemCellDelegate> delegate;

@end

@implementation ASOrderLineItemCell

#pragma mark - Init

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = [UIColor colorWithRed:0.965 green:0.973 blue:0.973 alpha:1.00];
    
    [self setupTextField];
    [self setupDottedLine];
    
    return self;
}

#pragma mark - Setup

- (void)setupTextField{
    
    _removeButton = [UIButton buttonWithType:UIButtonTypeSystem];
    _removeButton.tintColor = [UIColor favorBlue];
    [_removeButton setImage:[UIImage imageNamed:@"close_icon_small"] forState:UIControlStateNormal];
    [_removeButton addTarget:self action:@selector(dismissSelf) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_removeButton];
    
    UIButton *showMenu = [UIButton buttonWithType:UIButtonTypeSystem];
    [showMenu setTitle:@"View Menu" forState:UIControlStateNormal];
    [showMenu addTarget:self action:@selector(showMenu) forControlEvents:UIControlEventTouchUpInside];
    [showMenu setBackgroundColor: [UIColor colorWithRed:0.702 green:0.753 blue:0.776 alpha:1.00]];
    [showMenu setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [showMenu.titleLabel setFont:[UIFont fontWithName:@"AvenirNext-Regular" size:18.0f]];
    
    _textField = [[UITextField alloc] init];
    _textField.inputAccessoryView = showMenu;
    _textField.font = [UIFont fontWithName:@"AvenirNext-Regular" size:18.0];
    _textField.textColor = [UIColor darkGrayColor];
    _textField.delegate = self;
    [self addSubview:_textField];
}

- (void)setupDottedLine{

    _dottedLineLayer = [CAShapeLayer layer];
    [_dottedLineLayer setBounds:self.bounds];
    [_dottedLineLayer setPosition:self.center];
    [_dottedLineLayer setFillColor:[[UIColor clearColor] CGColor]];
    [_dottedLineLayer setStrokeColor:[[UIColor lightGrayColor] CGColor]];
    [_dottedLineLayer setLineWidth:1.0f];
    [_dottedLineLayer setLineJoin:kCALineJoinRound];
    [_dottedLineLayer setLineDashPattern:@[[NSNumber numberWithInt:2], [NSNumber numberWithInt:2]]];
    
    [self layoutDottedLine];
    
    [[self layer] addSublayer:_dottedLineLayer];
}

#pragma mark - Reuse

- (void)prepareForReuseWithText:(NSString *)text index:(NSInteger)index delegate:(id<ASOrderLineItemCellDelegate>)delegate{
    
    _delegate = delegate;
    _textField.text = text;
    _lineItemIndex = index;
}

#pragma mark - Layout

- (void)layoutSubviews{
    
    [super layoutSubviews];
    
    [self layoutDottedLine];
    
    CGFloat textFieldWidth = self.frame.size.width - kLeftTextFieldPadding - kMiddleTextFieldPadding - kCloseButtonWidth - kRightTextFieldPadding;
    _textField.frame = CGRectMake(kLeftTextFieldPadding, 0.0f, textFieldWidth, self.frame.size.height);
   
    CGFloat removeButtonOriginY = (self.frame.size.height - kCloseButtonWidth) / 2;
    _removeButton.frame = CGRectMake(textFieldWidth + kMiddleTextFieldPadding, removeButtonOriginY, kCloseButtonWidth, kCloseButtonWidth);
    
    _textField.inputAccessoryView.frame = CGRectMake(0.0f, 0.0f, self.frame.size.width, kViewMenuButtonHeight);
}

- (void)layoutDottedLine{
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, kLeftTextFieldPadding, self.frame.size.height - 1.0);
    CGPathAddLineToPoint(path, NULL, self.frame.size.width, self.frame.size.height - 1.0);
    [_dottedLineLayer setPath:path];
    CGPathRelease(path);
}

#pragma mark - Text Field Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - Helpers

- (void)dismissSelf{
    
    [_delegate removeLineItemAtIndex:_lineItemIndex];
}

- (NSString *)lineItemValue{
    
    return _textField.text;
}

- (void)resignTextField{
    
    if (_textField.isFirstResponder) [_textField resignFirstResponder];
}

- (void)makeTextFieldFirstResponder{
    
    [_textField becomeFirstResponder];
}

- (void)showMenu{
    
    [_delegate showMenu];
}

@end
