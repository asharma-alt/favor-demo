//
//  ASMenuItemTVC.h
//  Favor Demo
//
//  Created by Amit Sharma on 5/3/16.
//  Copyright © 2016 Amit Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ASMenuTableViewControllerDelegate <NSObject>

- (void)menuItemWasSelected:(NSString *)menuItem;

@end

@interface ASMenuItemTVC : UITableViewController

- (instancetype)initWithDelegate:(id<ASMenuTableViewControllerDelegate>)delegate menuItems:(NSArray *)menuItems;
- (void)updateTableViewTopContentInset:(CGFloat)topInset;

@end
