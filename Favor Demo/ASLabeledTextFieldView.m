//
//  ASLabeledTextFieldView.m
//  Favor Demo
//
//  Created by Amit Sharma on 5/2/16.
//  Copyright © 2016 Amit Sharma. All rights reserved.
//

#import "ASLabeledTextFieldView.h"

const static CGFloat kLabelRespondingHeightRatio = 0.4f;
const static CGFloat kTextFieldRespondingHeightRatio = 1.0 - kLabelRespondingHeightRatio;
const static CGFloat kLeftPadding = 15.0f;

@interface ASLabeledTextFieldView () <UITextFieldDelegate>

@property (nonatomic) BOOL transitionState;

@end

@implementation ASLabeledTextFieldView

#pragma mark - Init

- (instancetype)initWithTitle:(NSString *)title{
    
    self = [super init];
    
    _titleLabel = [[UILabel alloc] init];
    _titleLabel.text = title;
    _titleLabel.textColor = [UIColor lightGrayColor];
    [self addSubview:_titleLabel];
    
    _textField = [[UITextField alloc] init];
    _textField.font = [UIFont fontWithName:@"Avenir Next" size:22.0f];
    _textField.delegate = self;
    _textField.textColor = [UIColor darkGrayColor];
    [_textField addTarget:self action:@selector(textFieldChanged) forControlEvents:UIControlEventEditingChanged];
    [self addSubview:_textField];
    
    _clearButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [self addSubview:_clearButton];
    
    return self;
}

- (void)textFieldChanged{
    
    [self layoutSubviewsForIsFirstResponder:YES animated:YES];
}

#pragma mark - Layout

- (void)layoutSubviews{
    
    [super layoutSubviews];
    
    [self layoutSubviewsForIsFirstResponder:_textField.isFirstResponder animated:NO];
}

- (void)layoutSubviewsForIsFirstResponder:(BOOL)isFirstResponder animated:(BOOL)animated{
    
    if (animated) {
        
        [UIView animateWithDuration:0.2f animations:^{
            
            [self layoutSubviewsForIsFirstResponder];
        }];
    }
    
    else{
        
        [self layoutSubviewsForIsFirstResponder];
    }
}

- (void)layoutSubviewsForIsFirstResponder{
    
    if (_textField.text.length > 0) {
        
        _titleLabel.font = [UIFont fontWithName:@"Avenir Next" size:12.0f];
        _titleLabel.frame = CGRectMake(kLeftPadding, 5.0f, self.frame.size.width - kLeftPadding, self.frame.size.height * kLabelRespondingHeightRatio);
        _textField.frame = CGRectMake(kLeftPadding, _titleLabel.frame.size.height, self.frame.size.width, self.frame.size.height * kTextFieldRespondingHeightRatio);
    }
    
    else{
        
        _titleLabel.font = [UIFont fontWithName:@"Avenir Next" size:24.0f];
        _titleLabel.frame = CGRectMake(kLeftPadding, 0.0f, self.frame.size.width, self.frame.size.height);
        _textField.frame = CGRectMake(kLeftPadding, 0.0f, self.frame.size.width, self.frame.size.height);
    }
}

#pragma mark - UITextField Delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    [self layoutSubviewsForIsFirstResponder:_textField.isFirstResponder animated:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    [self layoutSubviewsForIsFirstResponder:_textField.isFirstResponder animated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}

@end
