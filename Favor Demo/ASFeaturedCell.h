//
//  ASFeaturedCell.h
//  Favor Demo
//
//  Created by Amit Sharma on 4/24/16.
//  Copyright © 2016 Amit Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASFeaturedContentPacket.h"

static NSString * kFeaturedCellReuseID = @"as_featured_cell";

@interface ASFeaturedCell : UITableViewCell

- (void)prepareForReuseWithFeaturedContentPacket:(ASFeaturedContentPacket *)contentPacket;

@end
