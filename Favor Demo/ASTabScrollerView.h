//
//  ASTabScrollerView.h
//  Amit Sharma
//
//  Created by Amit Sharma on 04/28/15.
//  Copyright © 2015 Amit Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASTabScrollerViewModes.h"

typedef void (^ASAnimationCompletion)();

@protocol ASTabScrollerDelegate <NSObject>

- (void)selectViewControllerAtIndex:(NSInteger)index completion:(ASAnimationCompletion)completion;

@end

@interface ASTabScrollerView : UIView

@property (weak, nonatomic) id<ASTabScrollerDelegate> delegate;

- (instancetype)initWithTabMode:(ASTabbedNavigationControllerScrollMode)tabMode titles:(NSArray *)titles options:(NSDictionary *)options;

@property (nonatomic, readonly) NSInteger selectedIndex;
@property (nonatomic) CGFloat suggestedMinimumHeight;

- (void)scrollToPercentageOfMaxContentOffset:(CGFloat)percentage;
- (void)scrollToPercentageToNextButton:(CGFloat)percentage;
- (void)selectButtonAtIndex:(NSInteger)index;

@end
