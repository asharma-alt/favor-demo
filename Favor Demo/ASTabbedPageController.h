//
//  ASTabbedPageController.h
//  Favor Demo
//
//  Created by Amit Sharma on 4/24/16.
//  Copyright © 2016 Amit Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASTabScrollerViewModes.h"

@interface ASTabbedPageController : UIViewController

@property (nonatomic, readonly) CGFloat centeredModeTopContentInset;

- (instancetype)initWithViewControllers:(NSArray *)controllers titles:(NSArray *)titles tabMode: (ASTabbedNavigationControllerScrollMode)tabMode options:(NSDictionary *)options;

@end
