//
//  ASFeaturedContentPacket.h
//  Favor Demo
//
//  Created by Amit Sharma on 5/8/16.
//  Copyright © 2016 Amit Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASStore.h"
#import "ASMenuItem.h"

@interface ASFeaturedContentPacket : NSObject

@property (strong, nonatomic) ASStore *store;
@property (strong, nonatomic) ASMenuItem *menuItem;
@property (strong, nonatomic) NSString *imageURL;

@end
