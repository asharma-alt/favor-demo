//
//  AppDelegate.m
//  Favor Demo
//
//  Created by Amit Sharma on 4/24/16.
//  Copyright © 2016 Amit Sharma. All rights reserved.
//

#import "AppDelegate.h"
#import "ASCategoryBrowser.h"
#import "ASKeyboardStateTracker.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
   
    [ASKeyboardStateTracker sharedTracker]; // starts tracking
    
    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
    self.window.rootViewController = [self catergoryBrowserWithFakedData];
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (UINavigationController *)catergoryBrowserWithFakedData{
    
    ASCategoryBrowser *catergoryBrowser = [[ASCategoryBrowser alloc] initWithMockData];
    UINavigationController *NC = [[UINavigationController alloc] initWithRootViewController:catergoryBrowser];
    NC.navigationBar.translucent = NO;
    
    return NC;
}

@end
