//
//  ASAddNewLineItemCell.m
//  Favor Demo
//
//  Created by Amit Sharma on 5/3/16.
//  Copyright © 2016 Amit Sharma. All rights reserved.
//

#import "ASAddNewLineItemCell.h"

@interface ASAddNewLineItemCell ()

@property (strong, nonatomic) UIButton *addButton;
@property (weak, nonatomic) id<ASAddNewLineItemCellDelegate> delegate;

@end

@implementation ASAddNewLineItemCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = [UIColor colorWithRed:0.965 green:0.973 blue:0.973 alpha:1.00];
    
    _addButton = [UIButton buttonWithType:UIButtonTypeSystem];
    _addButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [_addButton setTitle:@"+ Tap to add an item" forState:UIControlStateNormal];
    [_addButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [_addButton addTarget:self action:@selector(addNewLineItem) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_addButton];
    
    return self;
}

- (void)layoutSubviews{
    
    [super layoutSubviews];
    
    CGFloat buttonWidth = [_addButton sizeThatFits:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)].width + 5.0f;
    _addButton.frame = CGRectMake(15.0f, 0.0f, buttonWidth, self.frame.size.height);
}

- (void)prepareForReuseWithDelegate:(id<ASAddNewLineItemCellDelegate>)delegate{
    
    _delegate = delegate;
}

- (void)addNewLineItem{
    
    [_delegate addNewLineItem];
}

@end
