//
//  ASKeyboardStateTracker.h
//  Favor Demo
//
//  Created by Amit Sharma on 5/3/16.
//  Copyright © 2016 Amit Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString *kLastKnowKeyboardHeightChanged = @"last_know_keyboard_height_changed";

@interface ASKeyboardStateTracker : NSObject

@property (nonatomic, readonly) CGFloat lastKnownKeyboardHeight;

+ (instancetype)sharedTracker;
+ (BOOL)isKeyboardShowing;

@end
