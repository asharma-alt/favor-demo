//
//  UIColor+favorColors.m
//  Favor Demo
//
//  Created by Amit Sharma on 5/9/16.
//  Copyright © 2016 Amit Sharma. All rights reserved.
//

#import "UIColor+favorColors.h"

@implementation UIColor (favorColors)

+ (UIColor *)favorBlue{
    
    return [UIColor colorWithRed:0.102 green:0.635 blue:0.863 alpha:1.00];
}

@end
