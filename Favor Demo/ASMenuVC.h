//
//  ASMenuVC.h
//  Favor Demo
//
//  Created by Amit Sharma on 5/3/16.
//  Copyright © 2016 Amit Sharma. All rights reserved.
//

#import "ASTabbedPageController.h"

@protocol ASMenuViewControllerDelegate <NSObject>

- (void)addMenuItemToOrder:(NSString *)menuItem;

@end

@interface ASMenuVC : ASTabbedPageController

- (instancetype)initWithDelegate:(id<ASMenuViewControllerDelegate>)delegate;

@end
