//
//  ASFeaturedTVC.m
//  Favor Demo
//
//  Created by Amit Sharma on 4/25/16.
//  Copyright © 2016 Amit Sharma. All rights reserved.
//

#import "ASFeaturedTVC.h"
#import "ASFeaturedCell.h"
#import "UIColor+favorColors.h"

const static CGFloat kFeaturedCellHeight = 200.0f;

@interface ASFeaturedTVC ()

@property (strong, nonatomic) NSArray *featuredContentPackets;
@property (weak, nonatomic) id<ASFeaturedTVCDelegate> delegate;

@property (nonatomic) BOOL isNearBottom;
@property (nonatomic) CGFloat maxTableContentOffsetY;

@end

@implementation ASFeaturedTVC

#pragma mark - Init

- (instancetype)initWithFeaturedContentPackets:(NSArray *)featuredContentPackets delegate:(id<ASFeaturedTVCDelegate>)delegate{

    self = [super init];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = NO;
    
    _featuredContentPackets = featuredContentPackets;
    _delegate = delegate;
    _isNearBottom = NO;
    
    return self;
}

#pragma mark - Setup

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setupTableView];
}

- (void)setupTableView{
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ASFeaturedCell" bundle:nil] forCellReuseIdentifier:kFeaturedCellReuseID];
    self.tableView.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, kOrderAnythingButtonHeight, 0.0f);
}

#pragma mark - Layout

- (void)viewDidLayoutSubviews{
    
    [super viewDidLayoutSubviews];
    _maxTableContentOffsetY = _featuredContentPackets.count * kFeaturedCellHeight - self.view.frame.size.height;
}

- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    [self forceUpdateNearBottomState];
}

#pragma mark - UITableViewDelegate / UITableViewDataSource

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
 
    ASFeaturedCell *cell = [tableView dequeueReusableCellWithIdentifier:kFeaturedCellReuseID];
    ASFeaturedContentPacket *contentPacket = [_featuredContentPackets objectAtIndex:indexPath.row];
    
    [cell prepareForReuseWithFeaturedContentPacket:contentPacket];

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return kFeaturedCellHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _featuredContentPackets.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [_delegate presentOrderViewControllerWithFeaturedContent:[_featuredContentPackets objectAtIndex:indexPath.row]];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{

    if (_isNearBottom && _maxTableContentOffsetY - scrollView.contentOffset.y > 40.0) {
        
        [self setIsNotNearBottomOfTableView:NO];
    }
    
    else if (!_isNearBottom && _maxTableContentOffsetY - scrollView.contentOffset.y < 40.0f){
        
        [self setIsNotNearBottomOfTableView:YES];
    }
}

#pragma mark - Near Bottom Management

- (void)forceUpdateNearBottomState{
    
    if (_maxTableContentOffsetY - self.tableView.contentOffset.y > 40.0) {
        
        [self setIsNotNearBottomOfTableView:NO];
    }
    
    else if (_maxTableContentOffsetY - self.tableView.contentOffset.y < 40.0f){
        
        [self setIsNotNearBottomOfTableView:YES];
    }
}

- (void)setIsNotNearBottomOfTableView:(BOOL)isNearBottom{
    
    if (isNearBottom) {
        
        if (_maxTableContentOffsetY < self.view.frame.size.height) return;
        
        _isNearBottom = YES;
        self.tableView.backgroundColor = [UIColor favorBlue];
        [_delegate tableViewHasScrolledNearBottom:YES];
    }
    
    else{
        
        _isNearBottom = NO;
        self.tableView.backgroundColor = [UIColor whiteColor];
        [_delegate tableViewHasScrolledNearBottom:NO];
    }
}

@end
